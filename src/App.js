import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Router from './navigation';

function App() {
  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>
  );
}

export default App;
