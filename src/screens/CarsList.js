import {useIsFocused} from '@react-navigation/native';
import React from 'react';
import {StatusBar, StyleSheet, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Header from '../components/Header';
import ListCars from '../components/ListCars';
import {COLORS, SIZES} from '../constants';

const CarsList = () => {
  const FocusAwareStatusBar = props => {
    const isFocused = useIsFocused();

    return isFocused ? <StatusBar {...props} /> : null;
  };
  return (
    <View style={styles.page}>
      <FocusAwareStatusBar
        barStyle="dark-content"
        backgroundColor={COLORS.white}
      />
      <SafeAreaView>
        {/* <ScrollView showsVerticalScrollIndicator={false}> */}
        <Header title="Daftar Mobil" />
        <ListCars />
        {/* </ScrollView> */}
      </SafeAreaView>
    </View>
  );
};

export default CarsList;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  body: {
    marginHorizontal: SIZES.padding4,
  },
});
