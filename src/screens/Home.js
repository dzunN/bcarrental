/* eslint-disable react-native/no-inline-styles */
import {useIsFocused} from '@react-navigation/native';
import React from 'react';
import {
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';
import CardFeatures from '../components/CardFeatures';
import HomeHeader from '../components/HomeHeader';
import ListCars from '../components/ListCars';
import {COLORS, Images, SIZES} from '../constants';
import featuresData from '../data/featuresList';

const Home = () => {
  const FocusAwareStatusBar = props => {
    const isFocused = useIsFocused();

    return isFocused ? <StatusBar {...props} /> : null;
  };
  const RenderFeatures = () => {
    return (
      <FlatList
        data={featuresData}
        numColumns={4}
        columnWrapperStyle={{justifyContent: 'space-around'}}
        keyExtractor={item => item.id.toString}
        renderItem={({item}) => {
          return <CardFeatures item={item} />;
        }}
        style={{marginTop: SIZES.largeTitle * 2}}
      />
    );
  };

  return (
    <View style={styles.page}>
      <SafeAreaView>
        <FocusAwareStatusBar
          barStyle="dark-content"
          backgroundColor={COLORS.youngBlue}
        />
        <ScrollView>
          <HomeHeader
            name="Dzun Nurroin"
            location="Probolinggo, East Java"
            source={Images.Avatar}
          />
          <RenderFeatures />
          <ListCars />
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
});
