import Home from './Home';
import Akun from './Akun';
import CarsList from './CarsList';
import Splash from './Splash';

export {Home, Akun, CarsList, Splash};
