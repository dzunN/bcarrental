import {StyleSheet, Text, Image, SafeAreaView, View} from 'react-native';
import React from 'react';
import Header from '../components/Header';
import {COLORS, FONTS, Images, SIZES} from '../constants';
import Button from '../components/Button';

const Akun = () => {
  return (
    <View style={styles.page}>
      <SafeAreaView>
        <Header title="Akun" />
        <View style={styles.container}>
          <Image source={Images.IlusAkun} style={styles.ilusBG} />
          <Text style={styles.textInfo}>
            Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR
            lebih mudah
          </Text>
          <Button title="Register" />
        </View>
      </SafeAreaView>
    </View>
  );
};

export default Akun;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  container: {
    marginHorizontal: SIZES.padding6,
    alignItems: 'center',
    marginBottom: 110,
    marginTop: 84,
    width: SIZES.width * 0.87,
    height: SIZES.height,
  },
  ilusBG: {
    width: SIZES.width * 0.87,
    height: SIZES.height * 0.3,
    marginBottom: SIZES.padding4,
  },
  textInfo: {
    ...FONTS.body4,
    marginBottom: SIZES.padding4,
  },
});
