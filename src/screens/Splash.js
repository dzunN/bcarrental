import React, {useEffect} from 'react';
import {ImageBackground, StyleSheet} from 'react-native';
import {Images} from '../constants';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 3000);
  });

  return (
    <ImageBackground source={Images.SplashB} style={styles.background}>
      {/* <Image source={Logo} style={styles.logo} /> */}
    </ImageBackground>
  );
};

export default Splash;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
