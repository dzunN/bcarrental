import React from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';

const Promo = () => {
  return (
    <View style={styles.container}>
      <View style={styles.informasiSaldo}>
        <View style={styles.text}>
          <Text style={styles.labelSaldo}>
            Sewa Mobil Berkualitas di kawasanmu
          </Text>
          <Text style={styles.valueSaldo}>Sewa Mobil</Text>
        </View>
      </View>
    </View>
  );
};

export default Promo;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#091B6F',
    padding: 17,
    marginHorizontal: 14,
    borderRadius: 10,
    marginTop: -windowHeight * 0.07,
    flexDirection: 'row',
  },

  text: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  informasiSaldo: {
    width: '60%',
  },
  labelSaldo: {
    fontSize: 20,
    color: '#fff',
  },
  valueSaldo: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  labelPoint: {
    fontSize: 12,
  },
  valuePoint: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#55CB95',
  },
});
