import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {IcBox, IcCamera, IcKey, IcTruck} from '../assets';

const ButtonIcon = ({title, type}) => {
  const Icon = () => {
    if (title === 'Sewa Mobil') return <IcTruck />;
    if (title === 'Oleh-Oleh') return <IcBox />;
    if (title === 'Penginapan') return <IcKey />;
    if (title === 'Wisata') return <IcCamera />;

    return <IcTruck />;
  };
  return (
    <TouchableOpacity style={styles.container(type)}>
      <View style={styles.button(type)}>
        <Icon />
      </View>
      <Text style={styles.text(type)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default ButtonIcon;

const styles = StyleSheet.create({
  container: type => ({
    marginBottom: type === 'layanan' ? 12 : 0,
    marginRight: type === 'layanan' ? 30 : 0,
  }),
  button: type => ({
    backgroundColor: '#E0F7EF',
    padding: type === 'layanan' ? 18 : 7,
    borderRadius: 10,
  }),
  text: type => ({
    fontSize: type === 'layanan' ? 14 : 10,
    textAlign: 'center',
    fontWeight: type === 'layanan' ? '300' : 'bold',
  }),
});
