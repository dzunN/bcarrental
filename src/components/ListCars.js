import React from 'react';
import {FlatList, Text, StyleSheet} from 'react-native';
import {FONTS, SIZES} from '../constants';
import carList from '../data/carList';
import CarItem from './CarItem';

const ListCars = () => {
  const renderHeader = () => {
    return (
      <>
        <Text style={styles.headerTitle}>Daftar Mobil Pilihan</Text>
      </>
    );
  };
  return (
    <>
      <FlatList
        ListHeaderComponent={renderHeader}
        data={carList}
        keyExtractor={item => item.id}
        renderItem={({item}) => {
          return <CarItem item={item} />;
        }}
        nestedScrollEnabled
      />
    </>
  );
};

export default ListCars;

const styles = StyleSheet.create({
  headerTitle: {
    ...FONTS.h5,
    marginHorizontal: SIZES.body4,
    marginVertical: SIZES.body4,
  },
});
