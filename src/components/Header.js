import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {COLORS, FONTS, SIZES} from '../constants';

const Header = ({title}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    paddingHorizontal: SIZES.padding4,
    paddingVertical: SIZES.padding5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    ...FONTS.body3,
    color: COLORS.black,
  },
});
