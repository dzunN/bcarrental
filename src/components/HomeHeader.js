/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {COLORS, FONTS, Images, SIZES} from '../constants';
import Button from './Button';
import Gap from './Gap';

const HomeHeader = ({name, location, source}) => {
  const RenderPromos = () => {
    return (
      //==============================================
      // Text, Button, Gambar mobil Dipecah/Dibedain
      //==============================================

      <View style={styles.PromoBanner}>
        <Text style={styles.titleBanner}>
          Sewa Mobil Berkualitas di Kawasanmu
        </Text>
        <Gap height={16} />
        <Button title="Sewa Mobil" />
        <View style={styles.mobilBanner}>
          <Images.ShapeImg />
          <Images.CarImg
            style={{
              position: 'absolute',
              right: 0,
              bottom: 0,
            }}
          />
        </View>
      </View>

      //==============================
      // Pakai Gambar
      //==============================

      // <>
      //   <Image source={Images.Banner} style={styles.imgPromo} />
      // </>
    );
  };
  return (
    <View style={styles.header}>
      <View style={styles.profileInfo}>
        <View>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.location}>{location}</Text>
        </View>
        <TouchableOpacity>
          <Image source={source} style={styles.profileAvatar} />
        </TouchableOpacity>
      </View>
      <RenderPromos />
    </View>
  );
};

export default HomeHeader;

const styles = StyleSheet.create({
  header: {
    width: SIZES.width,
    height: SIZES.height * 0.21,
    paddingHorizontal: SIZES.body3,
    backgroundColor: COLORS.youngBlue,
  },

  profileInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: SIZES.body2,
    alignItems: 'center',
  },

  name: {
    ...FONTS.body4,
    color: COLORS.black,
  },

  location: {
    ...FONTS.h4,
    color: COLORS.black,
  },

  profileAvatar: {
    height: SIZES.height * 0.045,
    width: SIZES.width * 0.077,
    borderRadius: SIZES.radius,
  },

  PromoBanner: {
    borderRadius: 8,
    // width: SIZES.width * 0.91,
    height: SIZES.height * 0.2,
    padding: SIZES.padding5,
    backgroundColor: COLORS.primary,
    justifyContent: 'center',
  },

  mobilBanner: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },

  titleBanner: {
    ...FONTS.body3,
    color: COLORS.white,
    maxWidth: 170,
  },
  bgMobil: {
    width: SIZES.width * 0.5,
    height: SIZES.height * 0.1,
  },
  imgPromo: {
    width: SIZES.width * 0.92,
    height: SIZES.height * 0.2,
  },
});
