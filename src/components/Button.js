import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {COLORS, FONTS, SIZES} from '../constants';

const Button = ({title}) => {
  return (
    <TouchableOpacity>
      <View style={styles.container}>
        <Text style={styles.textButton}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.secondary,
    borderRadius: 2,
    width: SIZES.width * 0.27,
    // height: SIZES.height * 0.06,
    paddingHorizontal: SIZES.padding4,
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    color: COLORS.white,
    ...FONTS.body4,
  },
});
