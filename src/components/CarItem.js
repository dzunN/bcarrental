import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {COLORS, FONTS, Icons, SIZES} from '../constants';

const CarItem = ({item}) => {
  return (
    <TouchableOpacity>
      <View style={styles.wrapper}>
        <Image source={item.image} style={styles.image} />
        <View>
          <Text style={styles.car}>{item.car}</Text>
          <View style={styles.iconList}>
            <Icons.Users style={styles.icon} />
            <Text style={styles.text}>{item.people}</Text>
            <Icons.BriefCase style={styles.icon} />
            <Text style={styles.text}>{item.bags}</Text>
          </View>
          <Text style={styles.price}>{item.price}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CarItem;

const styles = StyleSheet.create({
  wrapper: {
    padding: SIZES.padding4,
    borderRadius: 4,
    backgroundColor: COLORS.white,
    flexDirection: 'row',
    elevation: 2,
    shadowColor: COLORS.primary,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    marginHorizontal: SIZES.padding4,
    marginVertical: SIZES.base,
  },
  image: {
    marginVertical: 8,
    marginRight: 16,
  },
  car: {
    ...FONTS.body4,
    color: COLORS.black,
  },
  iconList: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  icon: {
    marginRight: 4,
  },
  text: {
    marginRight: SIZES.padding4,
  },
  price: {
    ...FONTS.body4,
    color: COLORS.secondary,
  },
});
