/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {COLORS, FONTS, Icons, SIZES} from '../constants';

const CardFeatures = ({item}) => {
  const Icon = () => {
    if (item.label === 'Sewa Mobil') {
      return <Icons.IcTruck />;
    }
    if (item.label === 'Oleh-Oleh') {
      return <Icons.IcBox />;
    }
    if (item.label === 'Penginapan') {
      return <Icons.IcKey />;
    }
    if (item.label === 'Wisata') {
      return <Icons.IcCamera />;
    }

    return <Icons.IcBox />;
  };
  return (
    <TouchableOpacity
      style={{
        marginBottom: SIZES.padding * 2,
        alignItems: 'center',
      }}
      onPress={() => console.log(item.label)}>
      <View
        style={{
          height: 65,
          width: 65,
          marginBottom: 5,
          borderRadius: 16,
          backgroundColor: COLORS.lightGreen,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Icon />
      </View>
      <Text style={{textAlign: 'center', ...FONTS.body5}}>{item.label}</Text>
    </TouchableOpacity>
  );
};

export default CardFeatures;
