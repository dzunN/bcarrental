import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Icons, COLORS} from '../constants';

const TabItems = ({isFocused, onPress, onLongPress, label}) => {
  const Icon = () => {
    if (label === 'Home') {
      return isFocused ? <Icons.IcHomeActive /> : <Icons.IcHome />;
    }
    if (label === 'Daftar Mobil') {
      return isFocused ? <Icons.IcListActive /> : <Icons.IcList />;
    }
    if (label === 'Akun') {
      return isFocused ? <Icons.IcAkunActive /> : <Icons.IcAkun />;
    }

    return <Icons.IcHome />;
  };

  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      <Icon />
      <Text style={styles.text(isFocused)}>{label}</Text>
    </TouchableOpacity>
  );
};

export default TabItems;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },

  text: isFocused => ({
    fontSize: 13,
    color: isFocused ? COLORS.primary : COLORS.black,
    fontWeight: isFocused ? 'bold' : 'normal',
    marginTop: 8,
  }),
});
