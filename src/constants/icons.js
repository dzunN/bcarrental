import IcHome from '../assets/icons/fi_home.svg';
import IcHomeActive from '../assets/icons/fi_home_active.svg';
import IcList from '../assets/icons/fi_list.svg';
import IcListActive from '../assets/icons/fi_list_active.svg';
import IcAkun from '../assets/icons/fi_akun.svg';
import IcAkunActive from '../assets/icons/fi_akun_active.svg';

import IcTruck from '../assets/icons/fi_truck.svg';
import IcBox from '../assets/icons/fi_box.svg';
import IcKey from '../assets/icons/fi_key.svg';
import IcCamera from '../assets/icons/fi_camera.svg';

import BriefCase from '../assets/icons/briefCase.svg';
import Users from '../assets/icons/users.svg';

export default {
  IcHome,
  IcHomeActive,
  IcAkun,
  IcAkunActive,
  IcList,
  IcListActive,
  IcBox,
  IcCamera,
  IcKey,
  IcTruck,
  BriefCase,
  Users,
};
