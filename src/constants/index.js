import Icons from './icons';
import Images from './images';
import theme, {COLORS, SIZES, FONTS} from './theme';

export {Icons, Images, theme, COLORS, SIZES, FONTS};
