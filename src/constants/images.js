import SplashB from '../assets/images/Splash.png';
import Avatar from '../assets/images/Avatar.png';
import Banner from '../assets/images/Banner.png';
import HeaderBg from '../assets/images/headerBackground.png';
import CarImg from '../assets/images/MercedesBanner.svg';
import ShapeImg from '../assets/images/bg.svg';
import Mercedes from '../assets/images/Mercedes.png';
import IlusAkun from '../assets/images/Allura.png';

export default {
  SplashB,
  Avatar,
  Banner,
  HeaderBg,
  CarImg,
  ShapeImg,
  Mercedes,
  IlusAkun,
};
